import type { User } from '@/types/User'
import http from './http'

function addUser(user: User){
    return http.post('/users', user)
}

function updateUser(user: User){
    return http.patch(`/users/${user.id}`, user)
}

function delUser(user: User){
    return http.delete(`/users/${user.id}`)
}

function getUser(id: number){
    return http.delete(`/user/${id}`)
}

function getUsers(){
    return http.get('/user')
}

export default {addUser, updateUser, delUser, getUser, getUsers}