import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import http from '@/services/http'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStore =useLoadingStore()
  const users = ref<User[]>([])
  async function getUser(id:number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finish()
  }
  async function getUsers() {
    loadingStore.doLoad()
    const res = await http.get('/users')
    users.value = res.data
    loadingStore.finish()
  }
  async function saveUser(user: User) {
    loadingStore.doLoad()
    if (user.id < 0) {
      // add new
      console.log('Post' + JSON.stringify(user))
      const res = await http.post('/users', user)
    } else {
      //update
      console.log('Patch' + JSON.stringify(user))
      const res = await http.patch(`/users/${user.id}`, user)
    }
    await getUsers()
    loadingStore.finish()
  }
  
  async function deleteUser(user: User) {
    loadingStore.doLoad()
    const res = await http.delete(`/users/${user.id}`)
    await getUsers()
    loadingStore.finish()
  }

  return { users,getUsers,saveUser,deleteUser}
})
